package de.drayke;

import com.minecraftlegend.inventoryapi.Router.Router;
import de.drayke.commands.account.AccountCommand;
import de.drayke.commands.git.GitCommand;
import de.drayke.config.GitRemotes;
import de.drayke.config.MainConfig;
import de.drayke.gui.CloneGUI;
import de.drayke.gui.RemoteGUI;
import de.drayke.manager.AccountManager;
import de.drayke.manager.RemoteManager;
import de.drayke.manager.MavenBuilder;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * <h1>PluginBuilder</h1>
 * This Class aims to be the
 * Main class and the first on
 * to be launched after Bukkit started
 *
 * @author Drayke
 * @version 1.0
 * @since 15.12.2016
 */
public class PluginBuilder extends JavaPlugin
{

    private static PluginBuilder instance;

    @Getter
    private static MainConfig mainConfig;
    @Getter
    private static GitRemotes bookmarks;

    @Override
    public void onEnable() {
        instance = this;

        /**
         * Config init
         */
        mainConfig = new MainConfig(this);
        try {
            mainConfig.init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        initializeManager();

        registerCommands();

        registerUI();

    }

    private void registerUI()
    {
        Router.getInstance().registerGUI( CloneGUI.class );
        Router.getInstance().registerGUI( RemoteGUI.class );
    }

    private void initializeManager()
    {
        RemoteManager.getInstance().init();
        AccountManager.getInstance().init();
        MavenBuilder.getInstance().prepareRepositoryDirctory();
    }

    private void registerCommands()
    {
        new GitCommand().register(this );
        new AccountCommand().register( this );
    }

    @Override
    public void onDisable() {
        //Nothing to do
    }


    /**
     * Gets instance of plugin
     * @return JavaPlugin instance
     */
    public static PluginBuilder getInstance() {
        return instance;
    }
}
