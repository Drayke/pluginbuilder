package de.drayke.util.reflect;

/**
 * <h1>Modular-Server-System</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 19.09.2017
 */
public final class PacketReflectionException extends Exception
{

    public PacketReflectionException( Exception cause, String message)
    {
        super(message, cause);
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/