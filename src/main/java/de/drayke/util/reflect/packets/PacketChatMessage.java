package de.drayke.util.reflect.packets;


import de.drayke.util.reflect.Packet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.minecraftlegend.inventoryapi.utils.PackageType.MINECRAFT_SERVER;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 28.02.2017
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class PacketChatMessage implements Packet
{

    private String serializeString;

    @Override
    public String getClassName()
    {
        return "PacketPlayOutChat";
    }

    @Override
    public Object[] getConstructorObjects()
    {
        Object chatbase = null;
        try
        {
            Class<?> serialize = MINECRAFT_SERVER.getClass( "IChatBaseComponent" ).getClasses()[0];
            chatbase = serialize.getMethod( "a", String.class ).invoke( null, getSerializeString() );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return new Object[] { chatbase };
    }

}
