package de.drayke.util.reflect;

import org.bukkit.entity.Player;
import static de.drayke.util.reflect.PackageType.MINECRAFT_SERVER;


/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 28.02.2017
 */
public interface Packet
{

    /**
     * Should return the package of the related nms packet<br>
     * Its by default set to MINECRAFT_SERVER, because most packet are in this package
     *
     * @return the package of this packet
     */
    default PackageType getPackage()
    {
        return MINECRAFT_SERVER;
    }

    /**
     * Should return the correct classname of this packet
     *
     * @return the classname of thi packet
     */
    String getClassName();

    /**
     * Should return all required instanciation objects in the correct order
     *
     * @return all objects for every constructor parameter
     */
    Object[] getConstructorObjects();

    /**
     * Returns the class of this packet<br>
     * This method is not meant to be overwritten.
     *
     * @return the class of this packet
     *
     * @throws ClassNotFoundException
     */
    default Class getPacketClass() throws ClassNotFoundException
    {
        return getPackage().getClass( getClassName() );
    }

    /**
     * This method is used to edit the packet instance after instanciating<br>
     * Its meant to be overwritten<br>
     *
     * @param instance the clean packet instance
     *
     * @return the potentially edited packet instance
     *
     * @throws PacketReflectionException if something went wrong
     */
    default Object postConstruct( Object instance ) throws PacketReflectionException
    {
        return instance;
    }

    /**
     * This method is not meant to be overwritten<br>
     * Its used by the PacketReflection to instanciate this packet.
     *
     * @return a clean instance of this packet
     *
     * @throws PacketReflectionException if something went wrong
     */
    default Object construct() throws PacketReflectionException
    {
        try
        {
            Class packet = getPacketClass();
            return Reflect.instantiateObject( packet, getConstructorObjects() );
        }
        catch ( Exception e )
        {
            throw new PacketReflectionException( e, "something while constructing: " + this );
        }
    }

    /**
     * This method is meant to be called from outside to send this packet to a specific player
     *
     * @param player the player instance
     *
     * @throws PacketReflectionException if something went wrong while sending or creating the packet
     */
    default void send( Player player ) throws PacketReflectionException
    {
        PacketReflection.getInstance().sendReflectPacket( player, this );
    }

}
