package de.drayke.util.reflect;

import lombok.Getter;
import org.bukkit.entity.Player;

import static com.minecraftlegend.inventoryapi.utils.PackageType.CRAFTBUKKIT_ENTITY;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 28.02.2017
 */
public final class PacketReflection
{

    @Getter
    private static PacketReflection instance = new PacketReflection();

    private PacketReflection()
    {
    }

    public void sendReflectPacket( Player player, Packet packet ) throws PacketReflectionException
    {
        Object instance;
        try
        {
            instance = packet.construct();
            instance = packet.postConstruct( instance );
        }
        catch ( Exception e )
        {
            throw new PacketReflectionException( e, "failed to construct packet \"" + packet.getClassName() + "\" for player \"" + player.getName() + "\"" );
        }
        sendNMSPacket( player, instance );
    }

    /**
     * Attention: wrong packets can cause many errors
     *
     * @param player the bukkit player instance
     * @param packet the bukkit packet
     */
    public void sendNMSPacket( Player player, Object packet ) throws PacketReflectionException
    {
        if ( packet == null || player == null )
            throw new PacketReflectionException( new NullPointerException( "packet or player is null" ), "failed to send a packet to player \"" + player + "\"" );
        try
        {
            Object craftplayer = CRAFTBUKKIT_ENTITY.getClass( "CraftPlayer" ).cast( player );
            Object handle = Reflect.invokeMethod( craftplayer, "getHandle" );
            Object connection = Reflect.getValue( handle, true, "playerConnection" );
            Reflect.invokeMethod( connection, "sendNMSPacket", packet );
        }
        catch ( Exception e )
        {
            throw new PacketReflectionException( e, "failed to send a packet to player \"" + player.getName() + "\"" );
        }
    }

}
