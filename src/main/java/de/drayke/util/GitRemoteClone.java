package de.drayke.util;

import de.drayke.core.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eclipse.jgit.api.Git;
import java.io.File;
import java.io.IOException;

/**
 * <h1>GitRemoteClone</h1>
 * [The GitRemoteClone description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */

@AllArgsConstructor
public class GitRemoteClone
{
    @Getter
    private final String remoteURL;

    public boolean execute( Account account )
    {
        if ( account==null ) return false;
        try
        {
        // prepare a new folder for the cloned repository
        File localPath = File.createTempFile("GitRepository", "");
        if(!localPath.delete()) {
            throw new IOException("Could not delete temporary file " + localPath);
        }

        // then clone
        MessageCore.log("Cloning from " + this.remoteURL + " to " + localPath);
        try (Git result = Git.cloneRepository()
                .setCredentialsProvider( account.getCredentials() )
                .setRemote( this.remoteURL )
                .setDirectory(localPath)
                .call()) {
                // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!


                MessageCore.log("Having repository: " + result.getRepository().getDirectory());
                result.getRepository().close();
            }
        }
        catch ( Exception e )
        {

            return false;
        }
        return true;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/