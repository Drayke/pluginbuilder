package de.drayke.util;

import lombok.AllArgsConstructor;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * <h1>Crypter</h1>
 * [The Crypter description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
@AllArgsConstructor
public final class Crypter
{

    private final String loginPassword;

    public String hash()
    {
        try
        {
            return new String(getHashValue(),"UTF-8");
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return null;
    }

    public String decode(String password)
    {
        try
        {
            // BASE64 String zu Byte-Array konvertieren
            BASE64Decoder myDecoder2 = new BASE64Decoder();
            byte[] crypted2 = myDecoder2.decodeBuffer(password);

            // Entschluesseln
            Cipher cipher2 = Cipher.getInstance("AES");
            cipher2.init(Cipher.DECRYPT_MODE, getKey());
            byte[] cipherData2 = cipher2.doFinal(crypted2);
            return new String(cipherData2);
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return null;
    }

    public String encode(String password)
    {
        try
        {
            // Verschluesseln
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, getKey());
            byte[] encrypted = cipher.doFinal(password.getBytes());

            // bytes zu Base64-String konvertieren (dient der Lesbarkeit)
            BASE64Encoder myEncoder = new BASE64Encoder();
            String geheim = myEncoder.encode(encrypted);
            return geheim;
        }
        catch ( Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private SecretKeySpec getKey() throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
        byte[] key = getHashValue();
        // nur die ersten 128 bit nutzen
        key = Arrays.copyOf(key, 16);
        // der fertige Schluessel
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    private byte[] getHashValue() throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
        // byte-Array erzeugen
        byte[] key = this.loginPassword.getBytes("UTF-8");
        // aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        key = sha.digest(key);
        return key;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/