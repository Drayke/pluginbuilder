package de.drayke.util;

import de.drayke.PluginBuilder;
import de.drayke.config.MainConfig;
import de.drayke.core.Account;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * <h1>PluginBuilder</h1>
 * The Message enum for translation.
 *
 * @author Juyas, Drayke
 * @version 1.0
 * @since 15.12.2016
 */
public final class MessageCore
{

    private static MainConfig cfg = PluginBuilder.getMainConfig();

    public static final void message( Account sender, String... message )
    {
        MessageCore.message( Bukkit.getPlayer( sender.getPlayer() ), message );
    }

    public static final void message( CommandSender sender, String... message )
    {
        for ( String s : message )
        {
            sender.sendMessage( ChatColor.translateAlternateColorCodes( '&',cfg.getPrefix() + " " +  s ) );
        }
    }

    public static final void console( String... message )
    {
        for ( String s : message )
        {
            Bukkit.getConsoleSender().sendMessage( ChatColor.translateAlternateColorCodes( '&',cfg.getPrefix() + " " +  s ) );
        }
    }


    public static final void log( String... message )
    {
        for ( String s : message )
        {
            Bukkit.getConsoleSender().sendMessage( ChatColor.translateAlternateColorCodes( '&',cfg.getPrefix() + "§7: " + s ));
        }
    }
}
