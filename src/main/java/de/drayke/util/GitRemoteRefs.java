package de.drayke.util;

import de.drayke.core.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.CredentialsProvider;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * <h1>GitRemoteRefs</h1>
 * [The GitRemoteRefs description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
@AllArgsConstructor
public class GitRemoteRefs
{

    @Getter
    private final String remoteURL;

    public boolean execute( Account account)
    {
        if ( account==null ) return false;
        try
        {
            File localPath = File.createTempFile( "GitRepository", "" );
            if ( !localPath.delete() )
            {
                MessageCore.log( "Could not delete temporary file " + localPath );
            }

            CredentialsProvider cp = account.getCredentials();
            Collection<Ref> remoteRefs = Git.lsRemoteRepository()
                    .setCredentialsProvider( cp )
                    .setRemote( this.remoteURL )
                    .setTags( true )
                    .setHeads( true )
                    .call();
            for ( Ref ref : remoteRefs )
            {
                MessageCore.message(account, ref.getName() + " -> " + ref.getObjectId().name() );
            }

        }
        catch ( Exception e )
        {
            return false;
        }
        return true;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/