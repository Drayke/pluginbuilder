package de.drayke.util.chat;

import org.bukkit.entity.Player;

/**
 * <h1>legend-awakening</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 01.05.2017
 */
public final class EasyMessage
{

    private ChatMessageBuilder builder = new ChatMessageBuilder();

    /**
     * Adds a simple text with usual color support
     *
     * @param text the text
     *
     * @return instance again -> factory
     */
    public EasyMessage text( String text )
    {
        builder = builder.addSimpleText( text );
        return this;
    }

    /**
     * Adds a simple text with a second text, which will be displayed if the player hovers over this text
     *
     * @param msgText   the main display text
     * @param hoverText the text displayed if the player hovers over the message
     *
     * @return instance again -> factory
     */
    public EasyMessage hoverText( String msgText, String hoverText )
    {
        builder = builder.addSimpleHoverText( msgText, hoverText );
        return this;
    }

    /**
     * Adds a simple text, which will execute a command if the player clicks on this message
     *
     * @param msgText the main display text
     * @param command the command executed when clicking
     *
     * @return instance again -> factory
     */
    public EasyMessage clickCommand( String msgText, String command )
    {
        builder = builder.addSimpleClickCommand( msgText, command );
        return this;
    }

    /**
     * Adds a simple text, which will try to open an internet link if the player clicks on this message
     *
     * @param msgText the main display text
     * @param url     the URL
     *
     * @return instance again -> factory
     */
    public EasyMessage clickLink( String msgText, String url )
    {
        builder = builder.addSimpleURL( msgText, url );
        return this;
    }

    /**
     * Adds a simple text, which will give the player a command suggestion pasted in his input line if he clicks on this message
     *
     * @param msgText the main display text
     * @param suggest the command suggestion
     *
     * @return instance again -> factory
     */
    public EasyMessage clickSuggestion( String msgText, String suggest )
    {
        builder = builder.addMarked( new ChatMessagePart( msgText ).add( suggest, MessageReaction.SUGGEST_COMMAND, MessageGUIEvent.CLICK ) );
        return this;
    }

    /**
     * Adds a simple text with the features of a hover-text and a click-command
     *
     * @param msgText   the main display text
     * @param hoverText the text displayed on hovering
     * @param command   the command executed on clicking
     *
     * @return instance again -> factory
     */
    public EasyMessage hoverTextNclickCommand( String msgText, String hoverText, String command )
    {
        ChatMessagePart part = new ChatMessagePart( msgText );
        builder = builder.addMarked( part.add( hoverText, MessageReaction.SHOW_TEXT, MessageGUIEvent.HOVER ).add( command, MessageReaction.RUN_COMMAND, MessageGUIEvent.CLICK ) );
        return this;
    }

    /**
     * Adds a hover event reaction to the last added text. This will overwrite an existing hoverevent-reaction
     *
     * @param reaction the type of reaction
     * @param value    the value for the reaction
     *
     * @return instance again -> factory
     */
    public EasyMessage setHover( MessageReaction reaction, String value )
    {
        builder = builder.addMarkedOverwrite( builder.getLastPart().set( value, reaction, MessageGUIEvent.HOVER ) );
        return this;
    }

    /**
     * Adds a click event reaction to the last added text. This will overwrite an existing clickevent-reaction
     *
     * @param reaction the type of reaction
     * @param value    the value for the reaction
     *
     * @return instance again -> factory
     */
    public EasyMessage setClick( MessageReaction reaction, String value )
    {
        builder = builder.addMarkedOverwrite( builder.getLastPart().set( value, reaction, MessageGUIEvent.CLICK ) );
        return this;
    }

    /**
     * Will insert a line break
     *
     * @return instance again -> factory
     */
    public EasyMessage nextLine()
    {
        builder = builder.nextLine();
        return this;
    }

    /**
     * Will return the current length of this message
     *
     * @param ignoreColors whether the colors in this message should be ignored
     *
     * @return the length of the current message
     */
    public int getCurrentLength( boolean ignoreColors )
    {
        return builder.getMessageLength( !ignoreColors );
    }

    /**
     * Send this message to a player
     *
     * @param player the player
     */
    public void send( Player player )
    {
        builder.buildChatMessage().sendToPlayer( player );
    }

}