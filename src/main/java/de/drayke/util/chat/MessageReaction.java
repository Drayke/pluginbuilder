package de.drayke.util.chat;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 18.02.2017
 */
enum MessageReaction
{

    SHOW_TEXT( "show_text" ),
    RUN_COMMAND( "run_command" ),
    OPEN_URL( "open_url" ),
    SUGGEST_COMMAND( "suggest_command" );

    private final String action;

    MessageReaction( String action )
    {
        this.action = action;
    }

    public final String getAction()
    {
        return action;
    }

}
