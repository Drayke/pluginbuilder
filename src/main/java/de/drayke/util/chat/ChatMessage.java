package de.drayke.util.chat;


import de.drayke.util.reflect.packets.PacketChatMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 18.02.2017
 */
final class ChatMessage
{

    private String textBefore;
    private String[] marked;
    private String textAfter;

    private String[][] values;
    private MessageReaction[][] actions;
    private MessageGUIEvent[][] events;

    private ChatMessage nextLine;

    public ChatMessage( String textBefore, String textAfter, String[] textMarked, String[][] values, MessageReaction[][] actions, MessageGUIEvent[][] events, ChatMessage next )
    {
        this.actions = actions;
        this.events = events;
        this.marked = textMarked;
        this.textAfter = textAfter;
        this.textBefore = textBefore;
        this.values = values;
        this.nextLine = next;
    }

    public ChatMessage( ChatMessageBuilder builder )
    {
        this( builder.getBefore(), builder.getAfter(), builder.getMarked(), builder.getValues(), builder.getActions(), builder.getEvents(), null );
    }

    protected ChatMessage addLine( ChatMessage msg )
    {
        ChatMessage cm = this;
        while ( cm.nextLine != null )
        {
            cm = cm.nextLine;
        }
        cm.nextLine = msg;
        return this;
    }

    public int messageLength( boolean withColors )
    {

        int l = 0;

        if ( textBefore != null )
        {
            if ( withColors )
                l += textBefore.length();
            else l += ChatColor.stripColor( textBefore ).length();
        }
        if ( textAfter != null )
        {
            if ( withColors )
                l += textAfter.length();
            else l += ChatColor.stripColor( textAfter ).length();
        }

        if ( marked != null )
            for ( String m : marked )
            {
                if ( withColors )
                    l += m.length();
                else l += ChatColor.stripColor( m ).length();
            }

        return l;

    }

    public final String serializePacketString()
    {

        //before
        if ( textBefore == null ) textBefore = "";
        String packet = "{\"text\": \"" + textBefore + "\", \"extra\": [";

        for ( int i = 0; i < marked.length; i++ )
        {
            packet += createPart( i ) + ", ";
        }

        //after
        if ( textAfter != null )
            packet += "{ \"text\": \"" + textAfter + "\"}";
        else packet = packet.substring( 0, packet.length() - 2 );

        packet += "]}";

        return packet;
    }

    private final String createPart( int part )
    {

        String p = "{";

        p += valuePart( "text", marked[part] ) + ", ";

        if ( values[part] != null )
            if ( values[part].length > 0 )
                for ( int i = 0; i < values[part].length; i++ )
                {

                    MessageGUIEvent e = events[part][i];
                    MessageReaction a = actions[part][i];
                    String v = values[part][i];

                    if ( e != null && a != null && v != null )
                    {
                        p += v( e.getEventName() ) + ": {";
                        p += valuePart( "action", a.getAction() ) + ", ";
                        p += valuePart( "value", v );
                        p += "}, ";
                    }

                }

        p = p.substring( 0, p.length() - 2 );

        p += "}";

        return p;

    }

    private String valuePart( String call, String value )
    {
        return v( call ) + ": " + v( value );
    }

    private String v( String value )
    {
        return "\"" + value + "\"";
    }

    public void sendToPlayer( Player player )
    {
        PacketChatMessage msgPacket = new PacketChatMessage( this.serializePacketString() );
        try
        {
            msgPacket.send( player );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        if ( nextLine != null )
            nextLine.sendToPlayer( player );
    }

}
