package de.drayke.util.chat;

import org.bukkit.ChatColor;

import java.util.Vector;

/**
 * Do not use this old builder!
 * Use {@link EasyMessage} instead; it has better docs and should work much safer.
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 18.02.2017
 * @see EasyMessage
 * @deprecated its not recommended to use by yourself; its little bit buggy and allows many ways of functionality, which are inappropriate.
 */
@Deprecated
class ChatMessageBuilder
{

    public static int def_smoothLength = 55;
    public static int accept = 3;
    private ChatMessage lineBefore;
    private String messageBefore, messageAfter;
    private Vector<String> marked;
    private Vector<Vector<String>> values;
    private Vector<Vector<MessageReaction>> actions;
    private Vector<Vector<MessageGUIEvent>> events;

    public ChatMessageBuilder( ChatMessage lineBefore )
    {
        this( lineBefore, null, null );
    }

    public ChatMessageBuilder( ChatMessage lineBefore, String msgBefore, String msgAfter )
    {
        this.lineBefore = lineBefore;
        this.messageBefore = msgBefore;
        this.messageAfter = msgAfter;
        this.marked = new Vector<>();
        this.actions = new Vector<>();
        this.events = new Vector<>();
        this.values = new Vector<>();
    }

    public ChatMessageBuilder( String msgBefore, String msgAfter )
    {
        this( null, msgBefore, msgAfter );
    }

    public ChatMessageBuilder( String msgBefore )
    {
        this( null, msgBefore, null );
    }

    public ChatMessageBuilder()
    {
        this( null, null, null );
    }

    protected String getAfter()
    {
        return messageAfter;
    }

    protected String getBefore()
    {
        return messageBefore;
    }

    protected String[] getMarked()
    {
        if ( marked == null ) return new String[0];
        if ( marked.size() == 0 ) return new String[0];
        String[] m = new String[marked.size()];
        for ( int i = 0; i < m.length; i++ )
        {
            m[i] = marked.get( i );
        }
        return m;
    }

    protected String[][] getValues()
    {

        String[][] v = new String[values.size()][];

        if ( values.size() > 0 )
            for ( int i = 0; i < values.size(); i++ )
            {

                Vector<String> deep = values.get( i );

                v[i] = new String[deep.size()];

                if ( deep != null )
                    if ( deep.size() > 0 )
                        for ( int j = 0; j < deep.size(); j++ )
                        {
                            v[i][j] = deep.get( j );
                        }

            }

        return v;

    }

    protected MessageReaction[][] getActions()
    {

        MessageReaction[][] v = new MessageReaction[actions.size()][];

        if ( actions.size() > 0 )
            for ( int i = 0; i < actions.size(); i++ )
            {

                Vector<MessageReaction> deep = actions.get( i );

                v[i] = new MessageReaction[deep.size()];

                if ( deep != null )
                    if ( deep.size() > 0 )
                        for ( int j = 0; j < deep.size(); j++ )
                        {
                            v[i][j] = deep.get( j );
                        }

            }

        return v;

    }

    protected MessageGUIEvent[][] getEvents()
    {

        MessageGUIEvent[][] v = new MessageGUIEvent[events.size()][];

        if ( events.size() > 0 )
            for ( int i = 0; i < events.size(); i++ )
            {

                Vector<MessageGUIEvent> deep = events.get( i );

                v[i] = new MessageGUIEvent[deep.size()];

                if ( deep != null )
                    if ( deep.size() > 0 )
                        for ( int j = 0; j < deep.size(); j++ )
                        {
                            v[i][j] = deep.get( j );
                        }

            }

        return v;

    }

    public int getMessageLength()
    {
        return getMessageLength( false );
    }

    public int getMessageLength( boolean withcolor )
    {

        int l = 0;

        if ( messageAfter != null )
        {
            if ( withcolor )
                l += messageAfter.length();
            else l += ChatColor.stripColor( messageAfter ).length();
        }

        if ( messageBefore != null )
        {
            if ( withcolor )
                l += messageBefore.length();
            else l += ChatColor.stripColor( messageBefore ).length();
        }

        for ( String m : marked )
        {
            if ( withcolor )
                l += m.length();
            else l += ChatColor.stripColor( m ).length();
        }

        return l;

    }

    public ChatMessageBuilder nextLine()
    {
        return new ChatMessageBuilder( buildChatMessage( false ) );
    }

    public ChatMessageBuilder setMessageBefore( String messageBefore )
    {
        this.messageBefore = messageBefore;
        return this;
    }

    public ChatMessageBuilder setMessageAfter( String messageAfter )
    {
        this.messageAfter = messageAfter;
        return this;
    }

    public ChatMessageBuilder addSimpleText( String msg )
    {
        return addMarked( new ChatMessagePart( msg ) );
    }

    public ChatMessageBuilder addSimpleHoverText( String msg, String hover )
    {
        return addMarked( new ChatMessagePart( msg ).add( hover, MessageReaction.SHOW_TEXT, MessageGUIEvent.HOVER ) );
    }

    public ChatMessageBuilder addSimpleClickCommand( String msg, String command )
    {
        return addMarked( new ChatMessagePart( msg ).add( command, MessageReaction.RUN_COMMAND, MessageGUIEvent.CLICK ) );
    }

    public ChatMessageBuilder addSimpleURL( String msg, String url )
    {
        return addMarked(
                new ChatMessagePart( msg ).
                        add( url, MessageReaction.SHOW_TEXT, MessageGUIEvent.HOVER ).
                        add( url, MessageReaction.OPEN_URL, MessageGUIEvent.CLICK )
        );
    }

    public ChatMessageBuilder addMarked( ChatMessagePart part )
    {
        if ( part == null ) return this;
        if ( !part.isUsable() ) return this;
        marked.add( part.getTextPart() );
        values.add( part.getValues() );
        actions.add( part.getActions() );
        events.add( part.getEvents() );
        return this;
    }

    protected ChatMessageBuilder addMarkedOverwrite( ChatMessagePart part )
    {
        if ( part == null ) return this;
        if ( !part.isUsable() ) return this;
        marked.remove( marked.size() - 1 );
        values.remove( values.size() - 1 );
        actions.remove( actions.size() - 1 );
        events.remove( events.size() - 1 );
        return addMarked( part );
    }

    public ChatMessage buildChatMessage()
    {
        return buildChatMessage( false );
    }

    public ChatMessage buildChatMessage( boolean smooth )
    {
        if ( smooth ) return buildSmoothChatMessage();
        if ( lineBefore == null ) return new ChatMessage( this );
        return lineBefore.addLine( new ChatMessage( this ) );
    }

    protected ChatMessagePart getLastPart()
    {
        return decompilePart( values.size() - 1 );
    }

    protected ChatMessagePart decompilePart( int part )
    {
        ChatMessagePart p = new ChatMessagePart( marked.get( part ) );
        for ( int i = 0; i < values.get( part ).size(); i++ )
        {
            p.add( values.get( part ).get( i ), actions.get( part ).get( i ), events.get( part ).get( i ) );
        }
        return p;
    }

    protected boolean hasPart( int part )
    {
        return values.size() > part;
    }

    private ChatMessage buildSmoothChatMessage()
    {

        int length = def_smoothLength;

        ChatMessageBuilder cmb = new ChatMessageBuilder();

        int pos = 0;

        cmb.setMessageBefore( messageBefore );

        if ( cmb.getMessageLength() >= length )
        {
            cmb = cmb.nextLine();
        }

        while ( hasPart( pos ) )
        {

            if ( cmb.getMessageLength() < length )
            {
                cmb.addMarked( decompilePart( pos ) );
                pos++;
            }
            else
            {
                ChatMessagePart p = decompilePart( pos );
                if ( p.length() <= accept )
                {
                    cmb.addMarked( p );
                    pos++;
                }
                cmb = cmb.nextLine();
            }

        }

        if ( messageAfter != null )
            if ( cmb.getMessageLength() >= length )
            {
                cmb.setMessageAfter( messageAfter );
            }
            else
            {
                cmb = cmb.nextLine();
                cmb.setMessageAfter( messageAfter );
            }

        return cmb.buildChatMessage( false );

    }

}
