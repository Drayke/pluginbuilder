package de.drayke.util.chat;

import org.bukkit.ChatColor;

import java.util.Vector;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 18.02.2017
 */
final class ChatMessagePart
{

    private final String textPart;
    private Vector<String> values;
    private Vector<MessageReaction> actions;
    private Vector<MessageGUIEvent> events;

    public ChatMessagePart( String textPart )
    {
        this.textPart = textPart;
        this.values = new Vector<>();
        this.events = new Vector<>();
        this.actions = new Vector<>();
    }

    public ChatMessagePart add( String value, MessageReaction action, MessageGUIEvent event )
    {
        if ( !events.contains( event ) )
        {
            values.add( value );
            actions.add( action );
            events.add( event );
        }
        return this;
    }

    public ChatMessagePart set( String value, MessageReaction reaction, MessageGUIEvent event )
    {
        if ( events.contains( event ) )
        {
            int index = events.indexOf( event );
            values.remove( index );
            events.remove( index );
            actions.remove( index );
        }
        return add( value, reaction, event );
    }

    public int length()
    {
        return length( false );
    }

    public int length( boolean colors )
    {
        if ( colors )
            return textPart.length();
        else return ChatColor.stripColor( textPart ).length();
    }

    public String getTextPart()
    {
        return textPart;
    }

    public Vector<String> getValues()
    {
        return values;
    }

    public Vector<MessageGUIEvent> getEvents()
    {
        return events;
    }

    public Vector<MessageReaction> getActions()
    {
        return actions;
    }

    public boolean isUsable()
    {
        if ( textPart == null ) return false;
        return ( events.size() == actions.size() && actions.size() == values.size() );
    }

}
