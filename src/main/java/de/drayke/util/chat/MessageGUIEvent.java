package de.drayke.util.chat;

/**
 * @author Juyas, Drayke
 * @version 0.1
 * @since 18.02.2017
 */
enum MessageGUIEvent
{

    HOVER( "hoverEvent" ),
    CLICK( "clickEvent" );

    private final String eventName;

    MessageGUIEvent( String name )
    {
        this.eventName = name;
    }

    public final String getEventName()
    {
        return eventName;
    }

}
