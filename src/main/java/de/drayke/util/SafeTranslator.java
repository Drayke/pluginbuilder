package de.drayke.util;

import de.drayke.PluginBuilder;
import de.maddevs.translator.api.Dictionary;
import de.maddevs.translator.api.IArgument;
import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.Language;
import de.maddevs.translatorbukkit.api.LanguageResolver;
import org.bukkit.command.CommandSender;

/**
 * <h1>SafeTranslator</h1>
 * The SafeTranslator is a bridge for the actual
 * TranslationAPI. If the TranslationAPI is not enabled on
 * the target server, the tr() method still can be used. If that's
 * the case the original Text will be returned.
 *
 * @author Drayke
 * @version 1.0
 * @see Translator
 * @since 18.09.2017
 */
public final class SafeTranslator
{

    private static boolean isEnabled( boolean printError )
    {
        boolean enabled = PluginBuilder.getInstance().getServer().getPluginManager().isPluginEnabled( "TranslatorAPI" );
        if ( !enabled && printError )
            System.out.println( "TranslatorAPI is not enabled!" ); //TODO: Print your error message here.
        return enabled;
    }

    /**
     * Gets the selected Language of a minecraft player.
     * This method is just enabled when the TranslatorAPI is enabled.
     *
     * @param sender the sender
     *
     * @return the Language, if TranslatorAPI is not enabled: null
     *
     * @see LanguageResolver
     */
    public static Language lang( CommandSender sender )
    {
        if ( !isEnabled( false ) ) return null;
        return LanguageResolver.lang( sender );
    }

    /**
     * Gets the dictionary.
     *
     * @return the dictionary
     */
    public static Dictionary getDictionary()
    {
        if ( !isEnabled( true ) ) return null;
        return Dictionary.getInstance();
    }

    /**
     * Gets the language for a given language key.
     * Commonly the language key is a shortcut for the
     * language name. By default the Language "English" is
     * registered with the language key "en".
     *
     * @param languageKey the language key
     *
     * @return a language
     *
     * @see Language
     */
    public static Language getLanguage( String languageKey )
    {
        if ( !isEnabled( true ) ) return null;
        return Translator.getLanguage( languageKey );
    }

    /**
     * Adds an argument converter. Pay attention to use (@code hasArgumentConverter)
     * before to prevent unwanted conversions.
     *
     * @param argument the argument used for conversion
     */
    public static void addArgumentConverter( IArgument argument )
    {
        if ( !isEnabled( true ) ) return;
        Translator.addArgumentConverter( argument );
    }

    /**
     * Checks if a converter already is registered
     *
     * @param classType the class type
     *
     * @return true if available, else false
     */
    public static boolean hasArgumentConverter( Class<?> classType )
    {
        if ( !isEnabled( true ) ) return true;
        return Translator.hasArgumentConverter( classType );
    }

    /**
     * Gets the configured global language.
     * By default the Language is "English" with key: "en".
     *
     * @return the global language
     *
     * @see Language
     */
    public static Language globalLanguage()
    {
        if ( !isEnabled( true ) ) return null;
        return Translator.globalLanguage();
    }

    /**
     * Translates the original text into the given language.
     *
     * @param originalText the original text
     *
     * @return the string
     */
    public static String tr( String originalText )
    {
        return tr( globalLanguage(), originalText );
    }

    /**
     * Translates the original text into the given language.
     *
     * @param language     the language
     * @param originalText the original text
     *
     * @return the string
     */
    public static String tr( Language language, String originalText )
    {
        return tr( language.getLanguageKey(), originalText );
    }

    /**
     * Translates the original text into the given language.
     *
     * @param languageKey  the language key
     * @param originalText the original text
     *
     * @return the string
     */
    public static String tr( String languageKey, String originalText )
    {
        return tr( languageKey, originalText, new Object[0] );
    }

    /**
     * Translates the original text into the given language.
     *
     * @param originalText the original text
     * @param args         the args
     *
     * @return the string
     */
    public static String tr( String originalText, Object... args )
    {
        return tr( globalLanguage(), originalText, args );
    }

    /**
     * Translates the original text into the given language.
     *
     * @param language     the language
     * @param originalText the original text
     * @param args         the args
     *
     * @return the string
     */
    public static String tr( Language language, String originalText, Object... args )
    {
        return tr( language.getLanguageKey(), originalText, args );
    }

    /**
     * Translates the original text into the given language.
     * <ul>
     * <li>Without a language key, the default Language will be used to translate
     * the text.</li>
     * <li>Placeholders (@code Dictionary._ARG_) in the original text can be used
     * and replaced by arguments.</li>
     * <li>Arguments are replaced with the (@code toString()) method of the Object by default.</li>
     * <li>For better results: add some (@code IArgument) for the Converter or overwrite the (@code toString()) method</li>
     * <li>If no translation can be found for the given parameter, the original text will be returned</li>
     * </ul>
     *
     * @param languageKey  the language key
     * @param originalText the original text
     * @param args         the arguments
     *
     * @return the translated string
     *
     * @see Translator#addArgumentConverter(IArgument)
     * @see Dictionary#_ARG_ Dictionary#_ARG_
     */
    public static String tr( String languageKey, String originalText, Object... args )
    {
        String translation = originalText;

        if ( isEnabled( false ) )
            //Gets the translation. If no translation is available, the originalText will be returned
            return Translator.tr( languageKey, originalText, args );

        //Replace arguments
        String replacement = "";
        for ( Object obj : args )
        {
            replacement = obj.toString();
            translation = translation.replaceFirst( "##", replacement );
        }

        return translation;
    }


}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/