package de.drayke.backend;

/**
 * <h1>legend-awakening</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 04.05.2017
 */
public final class SQLDisconnectedException extends RuntimeException
{

    public SQLDisconnectedException( String message )
    {
        super( message );
    }

}
