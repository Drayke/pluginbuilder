package de.drayke.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <h1>Data Backend Controller</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 19.09.2017
 */
public final class SQLConnect
{

    private Connection sqlConnection;
    private boolean failOnDisconnect;
    private final MySQLConfig configuration;

    public SQLConnect( MySQLConfig config )
    {
        this.configuration = config;
        this.failOnDisconnect = false;
    }

    public boolean isConnected()
    {
        try
        {
            return sqlConnection != null && !sqlConnection.isClosed();
        }
        catch ( SQLException e )
        {
            return false;
        }
    }

    public boolean connect() throws SQLConnectionException
    {
        if ( !isConnected() )
            sqlConnection = getConnection();
        return isConnected();
    }

    private Connection getConnection() throws SQLConnectionException
    {
        if ( configuration == null || !checkDriver() ) return null;
        String path = getPath();
        try
        {
            return DriverManager.getConnection( path, configuration.getUser(), configuration.getPassword() );
        }
        catch ( SQLException e )
        {
            throw new SQLConnectionException( e, configuration );
        }
    }

    private String getPath()
    {
        return configuration.buildConnectorString();
    }

    public boolean call( String statement )
    {
        if ( !isConnected() )
        {
            if ( failOnDisconnect )
                throw new SQLDisconnectedException( "SQL-driver is actually disconnected; for automatic reconnecting, switch failOnDisconnect off" );
            else if ( !connect() )
                throw new SQLDisconnectedException( "SQL-driver failed to connect to database" );
            return false;
        }
        try
        {
            sqlConnection.prepareStatement( statement ).execute();
            return true;
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
            return false;
        }
    }

    public int callUpdate( String statement )
    {
        if ( !isConnected() )
        {
            if ( failOnDisconnect )
                throw new SQLDisconnectedException( "SQL-driver is actually disconnected; for automatic reconnecting, switch failOnDisconnect off" );
            else if ( !connect() )
                throw new SQLDisconnectedException( "SQL-driver failed to connect to database" );
            return -1;
        }
        try
        {
            return sqlConnection.prepareStatement( statement ).executeUpdate();
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
            return -1;
        }
    }

    public ResultSet callQuery( String statement )
    {
        if ( !isConnected() )
        {
            if ( failOnDisconnect )
                throw new SQLDisconnectedException( "SQL-driver is actually disconnected; for automatic reconnecting, switch failOnDisconnect off" );
            else if ( !connect() )
                throw new SQLDisconnectedException( "SQL-driver failed to connect to database" );
            return null;
        }
        try
        {
            return sqlConnection.prepareStatement( statement ).executeQuery();
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
            return null;
        }
    }

    public boolean disconnect()
    {
        try
        {
            if ( isConnected() )
            {
                if ( sqlConnection != null )
                    sqlConnection.close();
                sqlConnection = null;
            }
            return isConnected();
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
            return false;
        }
    }

    public boolean checkDriver()
    {
        try
        {
            Class.forName( MySQLConfig.DRIVER );
            return true;
        }
        catch ( ClassNotFoundException e )
        {
            return false;
        }
    }

    public void setFailOnDisconnect( boolean failOnDisconnect )
    {
        this.failOnDisconnect = failOnDisconnect;
    }

    public boolean willFailOnDisconnect()
    {
        return failOnDisconnect;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/