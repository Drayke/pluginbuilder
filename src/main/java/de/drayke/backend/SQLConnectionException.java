package de.drayke.backend;

/**
 * <h1>legend-awakening</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 04.05.2017
 */
public final class SQLConnectionException extends RuntimeException
{

    private MySQLConfig keySet;

    public SQLConnectionException( Exception cause, MySQLConfig configuration )
    {
        super( "connection failed caused by this configuration: " + configuration.toString(), cause );
        this.keySet = configuration;
    }

}