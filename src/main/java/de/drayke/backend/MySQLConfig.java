package de.drayke.backend;

import de.drayke.PluginBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.cubespace.Yamler.Config.Config;

/**
 * <h1>Data Backend Controller</h1>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 31.08.2017
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public final class MySQLConfig
{

    public static final String DRIVER = "com.mysql.jdbc.Driver";

    private String user = "user";

    private String password = "pw";

    public String buildConnectorString()
    {
        return "jdbc:sqlite:"+ PluginBuilder.getInstance().getDataFolder().getAbsolutePath()+"/accounts.db";
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/