package de.drayke.gui;

import com.minecraftlegend.inventoryapi.Elements.GUIButton;
import com.minecraftlegend.inventoryapi.Elements.GUILabel;
import com.minecraftlegend.inventoryapi.Elements.GUIPageButton;
import com.minecraftlegend.inventoryapi.Events.AnvilEvent;
import com.minecraftlegend.inventoryapi.Events.AnvilResultEvent;
import com.minecraftlegend.inventoryapi.Events.ComponentClickEvent;
import com.minecraftlegend.inventoryapi.GUIElement;
import com.minecraftlegend.inventoryapi.GUIEvent;
import com.minecraftlegend.inventoryapi.ItemBuilder;
import com.minecraftlegend.inventoryapi.Layouts.ListLayout;
import com.minecraftlegend.inventoryapi.McGui;
import com.minecraftlegend.inventoryapi.Router.annotation.Route;
import com.minecraftlegend.inventoryapi.utils.MenuRow;
import de.drayke.PluginBuilder;
import de.drayke.manager.RemoteManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>RemoteGUI</h1>
 * [The RemoteGUI description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
@Route( "/remote" )
public final class RemoteGUI extends McGui
{

    public RemoteGUI()
    {
        super( PluginBuilder.getInstance(),"Maps",new ListLayout( PluginBuilder.getInstance(),new ArrayList<>( ),3 ));
    }

    @Override
    public void draw( Player player ) {
        //draw is called once to open the GUI. The filter is null.
        //after a filter is applied, a query is executed on this instance. That's why draw() needs to be
        //called manually, but this time the filter is initialized!
        List<GUIElement> elements = elements( player );
        ListLayout listLayout = new ListLayout( PluginBuilder.getInstance(), elements, round( elements.size() ) / 9 );
        setLayout( listLayout );
        init();
        super.draw( player );
    }

    private List<GUIElement> elements( Player player )
    {
        //Menu
        MenuRow menuRow = new MenuRow();
        menuRow.setCloseBtnText( tr( lang( player ), "§cClose - ##", "Git Remotes" ) );
        menuRow.prepareMenu( player );

        //Item list
        ArrayList<GUIElement> result = new ArrayList<>( menuRow );
        for ( String remote : RemoteManager.getInstance().getRemotes() )
        {
            GUIPageButton button = new GUIPageButton( new ItemBuilder( "" ).type( Material.BOOKSHELF ).lore( remote ).build() ,"/remote/clone?repo="+remote);
            result.add( button );
        }

        return result;
    }


}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/