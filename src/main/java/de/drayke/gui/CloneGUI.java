package de.drayke.gui;

import com.minecraftlegend.inventoryapi.Elements.GUIProgressBar;
import com.minecraftlegend.inventoryapi.GUIContainer;
import com.minecraftlegend.inventoryapi.Layouts.ExactLayout;
import com.minecraftlegend.inventoryapi.Layouts.ListLayout;
import com.minecraftlegend.inventoryapi.Layouts.PlainLayout;
import com.minecraftlegend.inventoryapi.McGui;
import com.minecraftlegend.inventoryapi.Router.QueryParameter;
import com.minecraftlegend.inventoryapi.Router.annotation.Query;
import com.minecraftlegend.inventoryapi.Router.annotation.Route;
import com.minecraftlegend.inventoryapi.utils.Vector2i;
import de.drayke.PluginBuilder;
import de.drayke.core.Account;
import de.drayke.manager.AccountManager;
import de.drayke.util.GitRemoteClone;
import de.drayke.util.MessageCore;

import java.util.ArrayList;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>CloneGUI</h1>
 * [The CloneGUI description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
@Route( "/remote/clone" )
public final class CloneGUI extends McGui
{

    private GUIProgressBar progressBar;

    public CloneGUI()
    {
        super( PluginBuilder.getInstance(),"Cloning...",new PlainLayout());
        this.progressBar = new GUIProgressBar( new Vector2i(1,1),new Vector2i( 5,1 ) );
        add( this.progressBar );
    }


    //--> "/remote/clone?repo=bla.git"
    @Query( args = "repo")
    public void remote( QueryParameter repo )
    {
        Account acc = AccountManager.getInstance().getAccount( getPlayer() );
        if(acc==null)
        {
            MessageCore.message( getPlayer(),tr(lang(getPlayer()),"§cYou don't have an account yet! Please create an account first.") );
            return;
        }
        this.progressBar.setProgress( 10D );
        boolean execute = new GitRemoteClone( repo.get( 0 ) ).execute( acc );
        if(execute) this.progressBar.setProgress( 100D );
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/