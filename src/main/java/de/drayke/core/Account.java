package de.drayke.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.util.UUID;

/**
 * <h1>Account</h1>
 * [The Account description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
@AllArgsConstructor
public class Account
{
    @Getter
    private final UUID player;            //< The UUID of a Player
    private final String username;        //< The git username/email
    private final String password;        //< The password for git


    //TODO: Juyas --> SecurityManager :D
    public CredentialsProvider getCredentials()
    {
        return new UsernamePasswordCredentialsProvider(this.username,this.password);
    }
}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/