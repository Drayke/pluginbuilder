package de.drayke.manager;

import de.drayke.PluginBuilder;
import de.drayke.config.GitRemotes;
import de.drayke.util.MessageCore;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;

import java.util.List;

/**
 * <h1>RemoteManager</h1>
 * [The RemoteManager description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
public final class RemoteManager
{
    @Getter
    private static final RemoteManager instance = new RemoteManager();

    private RemoteManager(){
    }

    public void init()
    {
        if( gitRemotes !=null) return;

        gitRemotes = new GitRemotes( PluginBuilder.getInstance() );
        try
        {
            gitRemotes.init();
        }
        catch ( InvalidConfigurationException e )
        {
            MessageCore.log( "Could not load remotes!" );
            e.printStackTrace();
        }
    }

    @Getter
    private GitRemotes gitRemotes;

    public List<String> getRemotes()
    {
        return gitRemotes.getRemotes();
    }
}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/