package de.drayke.manager;

import de.drayke.PluginBuilder;
import de.drayke.util.MessageCore;
import lombok.Getter;
import org.apache.maven.cli.MavenCli;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

/**
 * <h1>PluginBuilder</h1>
 * The Message enum for translation.
 *
 * @author Juyas, Drayke
 * @version 1.0
 * @since 15.12.2016
 */
public final class MavenBuilder  {

    private static String pathToRepository = null;

    @Getter
    private static MavenBuilder instance = new MavenBuilder();

    private MavenBuilder(){}

    public void prepareRepositoryDirctory()
    {
        if(pathToRepository == null)
        {
            pathToRepository = PluginBuilder.getInstance().getDataFolder().getAbsolutePath() + File.pathSeparator + "repo" + File.pathSeparator;
            new File(pathToRepository);
            MessageCore.log("Repository path: " + pathToRepository);
        }
    }

    public String getPathToRepository() {
        return pathToRepository;
    }


    public void buildProject(File workingDirectory)
    {
        //TODO: maven-invoker ?! --> der kann pom.xml datein direkt ausführen. Aber ich glaube dafür muss maven installiert sein..
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        MavenCli cli = new MavenCli();

        int mvnRunResult = cli.doMain(new String[]{"clean","package"},
                workingDirectory.getAbsolutePath(),
                printStream,
                printStream);

        printStream.flush();
        System.out.println(getOutput(outputStream));


        if (mvnRunResult != 0) {
            throw new RuntimeException("Error while building Maven project from basedir " + workingDirectory.getAbsolutePath() +
                    ". Return code=" + mvnRunResult);
        }



    }

    private String getOutput(ByteArrayOutputStream output) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n\n==============================\n");
        builder.append("  MAVEN CONSOLE OUTPUT\n");
        builder.append("==============================\n");
        builder.append(new String(output.toByteArray()));
        builder.append("\n==============================\n\n");
        return builder.toString();
    }
}
