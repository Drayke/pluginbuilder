package de.drayke.manager;

import de.drayke.backend.MySQLConfig;
import de.drayke.backend.SQLConnect;
import de.drayke.core.Account;
import de.drayke.util.Crypter;
import de.drayke.util.MessageCore;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>AccountManager</h1>
 * [The AccountManager description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
public class AccountManager
{
    @Getter
    private static final AccountManager instance = new AccountManager();

    private AccountManager(){}

    //-----------------------------------------------------------

    private final String TABLE_NAME = "accounts";
    private final String TABLE_COLUMNS = "uuid char(36) PRIMARY KEY NOT NULL, login varchar(64), email varchar(255), password varchar(255)";

    private HashSet<Account> accounts;

    private HashMap<UUID,AccountFactory> factoryMap;

    private SQLConnect sql;

    /**
     * Init.
     */
    public void init()
    {
        if(sql!=null) return;
        sql = new SQLConnect( new MySQLConfig() );
        sql.connect();
        sql.call( "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+ "("+TABLE_COLUMNS+")");
    }


    /**
     * Returns a factory if the player don't have an account yet.
     *
     * @param player the player without account
     *
     * @return the Factory or null if player already has an account.
     */
    public AccountFactory factory(UUID player)
    {
        if(hasAccount( player )) return null;

        if(factoryMap.containsKey( player ))
            return factoryMap.get( player );

        AccountFactory factory = new AccountFactory( player );
        factoryMap.put( player, factory);

        return factory;
    }

    /**
     * Registers an Account with all required fields
     *
     * @param player   the player
     * @param login    the login psw
     * @param email    the git email
     * @param password the password
     *
     * @return a boolean indicator for success
     */
    public boolean register( UUID player, String login, String email, String password )
    {
        Crypter crypter = new Crypter( login );
        boolean call = sql.call( "INSERT INTO " + TABLE_NAME + "(uuid,login,email,password) VALUES ('" + player.toString() + "','" + crypter.hash() + "','" + email + "','" + crypter.encode( password ) + "'" );
        if(call)
        {
            Player p = Bukkit.getPlayer( player );
            MessageCore.message( p,tr( lang( p ),"§aPlease use /account login to login into your new account." ) );
            factoryMap.remove( player );
        }
        return call;
    }


    /**
     * Loads an account from the database if the login password is correct
     *
     * @param player   the player
     * @param password the login password
     *
     * @return boolean indicator for success
     */
    public boolean login(Player player, String password)
    {
        ResultSet resultSet = sql.callQuery( "SELECT * FROM " + TABLE_NAME + " WHERE uuid='" + player.getUniqueId().toString() + "'" );
        try
        {
            if(resultSet.next())
            {
                Crypter crypter = new Crypter( password );
                String login = resultSet.getString( "login" );
                String email = resultSet.getString( "email" );
                String psw = resultSet.getString( "password" );
                if(crypter.hash().equals( login ))
                {
                    accounts.add( new Account( player.getUniqueId(),email,psw ) );
                    MessageCore.message( player,tr(lang(player),"§aLogin successful!") );
                    return true;
                }
                else
                {
                    MessageCore.message( player,tr(lang(player),"§cWrong password!") );
                    return false;
                }
            }
        }
        catch ( SQLException e )
        {
            MessageCore.message( player,tr(lang(player),"§cError! Do you have an account?") );
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * Gets account.
     *
     * @param player the player
     *
     * @return the account
     */
    public Account getAccount(Player player)
    {
        return getAccount( player.getUniqueId() );
    }

    /**
     * Gets account.
     *
     * @param player the player
     *
     * @return the account
     */
    public Account getAccount(UUID player)
    {
        Optional<Account> first = accounts.stream().filter( account -> account.getPlayer().equals( player ) ).findFirst();
        if(first.isPresent())
            return first.get();
        return null;
    }

    /**
     * Has account boolean.
     *
     * @param player the player
     *
     * @return the boolean
     */
    public boolean hasAccount(Player player)
    {
        return hasAccount( player.getUniqueId() );
    }

    /**
     * Has account boolean.
     *
     * @param player the player
     *
     * @return the boolean
     */
    public boolean hasAccount(UUID player)
    {
        Optional<Account> first = accounts.stream().filter( account -> account.getPlayer().equals( player ) ).findFirst();
        return first.isPresent();
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/