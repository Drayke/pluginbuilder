package de.drayke.manager;

import de.drayke.util.MessageCore;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.UUID;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>AccountFactory</h1>
 * [The AccountFactory description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
@AllArgsConstructor
public class AccountFactory
{
    @Getter
    private UUID playerUUID;

    private String login;
    private String email;
    private String password;

    public AccountFactory(UUID playerUUID)
    {
        this.playerUUID = playerUUID;
    }

    public AccountFactory setPassword( String password )
    {
        this.password = password;
        return this;
    }

    public AccountFactory setLogin( String login )
    {
        this.login = login;
        return this;
    }

    public AccountFactory setEmail( String email )
    {
        this.login = login;
        return this;
    }

    public void printStatus( Player player )
    {
        MessageCore.message( player,
                tr(lang(player),"§7Login password: ##",valid( this.login )?"§a✔":"§c✖"),
                tr(lang( player ),"§7Git Repository email: ##",valid( this.email )?"§a✔":"§c✖"),
                tr(lang( player ),"§7Git Repository password: ##",valid( this.password )?"§a✔":"§c✖")
                );
    }

    public boolean isValid()
    {
        return playerUUID != null &&
                valid( login ) &&
                valid( email ) &&
                valid( password );
    }

    public boolean register()
    {
        if ( isValid() )
            return AccountManager.getInstance().register(playerUUID,login,email,password);
        else
            return false;
    }

    private boolean valid( String s )
    {
        return s != null && !s.isEmpty();
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/