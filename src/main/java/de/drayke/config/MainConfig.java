package de.drayke.config;

import de.drayke.PluginBuilder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;

import java.io.File;


/**
 * <h1>PluginBuilder</h1>
 * The MainConfig class
 *
 * @author Drayke
 * @version 1.0
 * @since 15.12.2016
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class MainConfig extends Config {

    @Comment("Prefix for plugin messages")
    private String prefix = "&9Plugin&3Builder&7>";



    /**
     * Instantiates a new Config
     *
     * @param plugin the plugin
     */
    public MainConfig(PluginBuilder plugin)
    {

        if(!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdir())
        {
            plugin.getLogger().severe("Could not load datafolder.");
            return;
        }

        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");
        CONFIG_MODE = ConfigMode.FIELD_IS_KEY;

    }
}
