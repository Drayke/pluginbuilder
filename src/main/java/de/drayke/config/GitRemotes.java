package de.drayke.config;

import de.drayke.PluginBuilder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;

import java.io.File;
import java.util.ArrayList;

/**
 * <h1>PluginBuilder</h1>
 * A class to safe used working links.
 *
 * @author Drayke
 * @version 1.0
 * @since 15.12.2016
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class GitRemotes extends Config {

    @Comment("This list contains already used, working remotes.")
    private ArrayList<String> remotes = new ArrayList<>();

    /**
     * Instantiates a new GitRemotes config
     *
     * @param plugin the plugin
     */
    public GitRemotes( PluginBuilder plugin)
    {

        if(!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdir())
        {
            plugin.getLogger().severe("Could not load datafolder.");
            return;
        }

        CONFIG_FILE = new File(plugin.getDataFolder(), "remotes.yml");
        CONFIG_MODE = ConfigMode.FIELD_IS_KEY;

    }
}
