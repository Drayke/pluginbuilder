package de.drayke.commands.git;

import com.minecraftlegend.inventoryapi.Router.Navigation;
import de.drayke.PluginBuilder;
import de.drayke.gui.RemoteGUI;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

/**
 * <h1>GitRemoteCommand</h1>
 * [The GitRemoteCommand description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
final class GitRemoteCommand extends SecondaryCommand<PluginBuilder>
{
    public GitRemoteCommand()
    {
        super(GitRemoteCommand.class);
        registerSubCommand( new GitRemoteSubCommands() );
    }

    @HelpPage( desc = "Command to handle the git remotes." )
    @Command( name = "remote", permission = "pb.git.remote")
    public void remote( @Param(param = EParam.PLAYER) Player player )
    {
        Navigation.getInstance().push( player, RemoteGUI.class );
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/