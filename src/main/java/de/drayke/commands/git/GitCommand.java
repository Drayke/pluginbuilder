package de.drayke.commands.git;

import de.drayke.PluginBuilder;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

/**
 * <h1>GitCommand</h1>
 * [The GitCommand description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
public class GitCommand extends PrimaryCommand<PluginBuilder>
{
    public GitCommand(){
        super(GitCommand.class);
        registerSubCommand( new GitRemoteCommand() );
    }

    @HelpPage( desc = "The Git commands." )
    @Command( name = "git" , permission = "pb.git")
    public void git( @Param(param = EParam.PLAYER) Player player )
    {

    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/