package de.drayke.commands.git;

import de.drayke.PluginBuilder;
import de.drayke.core.Account;
import de.drayke.manager.AccountManager;
import de.drayke.util.GitRemoteRefs;
import de.drayke.util.MessageCore;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>GitRemoteSubCommands</h1>
 * [The GitRemoteSubCommands description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
final class GitRemoteSubCommands extends SecondaryCommand<PluginBuilder>
{
    public GitRemoteSubCommands()
    {
        super(GitRemoteSubCommands.class);
    }


    @HelpPage( desc = "add description" )
    @Command( name = "add" )
    public void add( @Param(param = EParam.PLAYER) Player player, String remote )
    {
        Account acc = AccountManager.getInstance().getAccount( player );
        if(acc==null)
        {
            MessageCore.message( player,tr(lang(player),"§cYou don't have an account yet! Please create an account first.") );
            return;
        }
        if(!new GitRemoteRefs( remote ).execute( acc ))
            MessageCore.message( player,tr(lang( player ),"§cCould not fetch data from the remote git repository: §e##",remote) );
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/