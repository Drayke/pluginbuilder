package de.drayke.commands.account;

import com.minecraftlegend.inventoryapi.McOptionPane;
import de.drayke.PluginBuilder;
import de.drayke.manager.AccountFactory;
import de.drayke.manager.AccountManager;
import de.drayke.util.MessageCore;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>AccountCreateSubCommands</h1>
 * [The AccountCreateSubCommands description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
final class AccountCreateSubCommands extends SecondaryCommand<PluginBuilder>
{

    public AccountCreateSubCommands()
    {
        super( AccountCreateSubCommands.class );
    }

    @HelpPage(desc = "Sets the login password")
    @Command(name = "login", validArguments = 1, permission = "pb.account.create")
    public void login( @Param(param = EParam.PLAYER) Player player, String loginpassword )
    {
        AccountFactory factory = AccountManager.getInstance().factory( player.getUniqueId() );
        if ( factory != null )
        {
            McOptionPane.showConfirmDialog( PluginBuilder.getInstance(),
                    player,
                    tr( lang( player ), "Please check your login password again:§e ##!", loginpassword ),
                    tr( lang( player ), "§aAccept" ),
                    tr( lang( player ), "§cDeny" ),
                    componentClickEvent -> {
                        factory.setLogin( loginpassword ).printStatus( player );
                    },
                    componentClickEvent -> {
                        //nothing;
                    }
            );
        }
        else
        {
            MessageCore.message( player, tr( lang( player ), "§cYou can't create a new account." ) );
        }
    }

    @HelpPage(desc = "Sets the email for the remote git repositories")
    @Command(name = "email", validArguments = 1, permission = "pb.account.create")
    public void email( @Param(param = EParam.PLAYER) Player player, String email )
    {
        AccountFactory factory = AccountManager.getInstance().factory( player.getUniqueId() );
        if ( factory != null )
        {
            McOptionPane.showConfirmDialog( PluginBuilder.getInstance(),
                    player,
                    tr( lang( player ), "Please check your email again:§e ##!", email ),
                    tr( lang( player ), "§aAccept" ),
                    tr( lang( player ), "§cDeny" ),
                    componentClickEvent -> {
                        factory.setEmail( email ).printStatus( player );
                    },
                    componentClickEvent -> {
                        //nothing;
                    }
            );
        }
        else
        {
            MessageCore.message( player, tr( lang( player ), "§cYou can't create a new account." ) );
        }
    }

    @HelpPage(desc = "Sets the password for the remote git repositories")
    @Command(name = "password", validArguments = 1, permission = "pb.account.create")
    public void password( @Param(param = EParam.PLAYER) Player player, String password )
    {
        AccountFactory factory = AccountManager.getInstance().factory( player.getUniqueId() );
        if ( factory != null )
        {
            McOptionPane.showConfirmDialog( PluginBuilder.getInstance(),
                    player,
                    tr( lang( player ), "Please check your email password again:§e ##!", password ),
                    tr( lang( player ), "§aAccept" ),
                    tr( lang( player ), "§cDeny" ),
                    componentClickEvent -> {
                        factory.setPassword( password ).printStatus( player );
                    },
                    componentClickEvent -> {
                        //nothing;
                    }
            );
        }
        else
        {
            MessageCore.message( player, tr( lang( player ), "§cYou can't create a new account." ) );
        }
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/