package de.drayke.commands.account;

import de.drayke.PluginBuilder;
import de.drayke.manager.AccountFactory;
import de.drayke.manager.AccountManager;
import de.drayke.util.MessageCore;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

import static de.drayke.util.SafeTranslator.lang;
import static de.drayke.util.SafeTranslator.tr;

/**
 * <h1>AccountCreateCommand</h1>
 * [The AccountCreateCommand description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
final class AccountCreateCommand extends SecondaryCommand<PluginBuilder>
{
    public AccountCreateCommand()
    {
        super(AccountCreateCommand.class);
        registerSubCommand( new AccountCreateSubCommands() );
    }

    @HelpPage( desc = "Creates a new account. Use the three sub commands to configure and register your account! When all fields are valid, call \"/account create\" again.",
               usage = "/account create [login|email|password] <input>",
                perm = "pb.account.create")
    @Command( name = "create", aliases = "register", permission = "pb.account.create")
    public void create( @Param(param = EParam.PLAYER) Player player )
    {
        AccountFactory factory = AccountManager.getInstance().factory( player.getUniqueId() );
        if(factory!=null)
        {
            if(!factory.isValid())
            {
                factory.printStatus( player );
                MessageCore.message(player,tr( lang( player ),"§7Use /account create -help for more informations!" ));
            }
            else
            {
                if(!factory.register())
                    MessageCore.message( player,tr( lang( player ),"§cSomething went wrong registering your account!" ) );
            }
        }
        else
        {
            MessageCore.message( player,tr(lang(player),"§cYou can't create a new account.") );
        }
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/