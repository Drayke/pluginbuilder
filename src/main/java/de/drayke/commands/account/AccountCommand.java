package de.drayke.commands.account;

import de.drayke.PluginBuilder;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

/**
 * <h1>AccountCommand</h1>
 * [The AccountCommand description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
public class AccountCommand extends PrimaryCommand<PluginBuilder>
{

    public AccountCommand()
    {
        super( AccountCommand.class );
        registerSubCommand( new AccountSubCommands() );
        registerSubCommand( new AccountCreateCommand() );
    }

    @HelpPage( desc = "Your account." )
    @Command( name = "account" )
    public void account( @Param(param = EParam.PLAYER) Player player )
    {

    }


}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/