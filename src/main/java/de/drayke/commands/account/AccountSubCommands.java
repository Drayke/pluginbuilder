package de.drayke.commands.account;

import de.drayke.PluginBuilder;
import de.drayke.manager.AccountManager;
import de.maddevs.command.*;
import org.bukkit.entity.Player;

/**
 * <h1>AccountSubCommands</h1>
 * [The AccountSubCommands description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 30.10.2017
 */
final class AccountSubCommands extends SecondaryCommand<PluginBuilder>
{

    public AccountSubCommands()
    {
        super(AccountSubCommands.class);
    }

    @HelpPage( desc = "The Login for you account. Your settings will be loaded from the database!" )
    @Command( name = "login", validArguments = 1,permission = "pb.account.login")
    public void login( @Param(param = EParam.PLAYER) Player player, String password )
    {
        AccountManager.getInstance().login( player,password );
    }

    @HelpPage( desc = "Configures you account. The username and password for your git credentials can be set here." )
    @Command( name = "config",validArguments = 1,permission = "pb.account.config" )
    public void config( @Param(param = EParam.PLAYER) Player player )
    {

    }


}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/